Actividad | Generar una carpeta en varios servidores

En esta actividad nos vamos a dividir en equipos, vamos a configurar 4 servidores (bastion, Sales, Accounting y  HR) donde a partir de bastion que contiene ansible vamos a controlar el resto de los servidores. Con ansible generar un playbook que:

1) Genere una carpeta Alumnos

2) Dentro de la carpeta Alumnos genere un archivo con el nombre de cada participante del equipo.

Suba un archivo de ansible a gitlab y entregue la liga en la cuenta de cada miembro del equipo.

Melissa García Mendoza

Ana Marina Ortiz Ruíz
